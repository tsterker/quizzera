# Quizzera
### Reputation-Based Collaborative Course Revision Platform

========

## Installation

(1) Install Nodejs:

http://nodejs.org/download/

(2) Clone Quizzera repository and install dependencies:
```
git clone git@github.com:tsterker/quizzera.git
cd quizzera
sudo npm install
```
(3) Start Server
```
sails lift
```

(see http://sailsjs.org/#/documentation/concepts/Deployment for more information about deploying SailsJS apps)
